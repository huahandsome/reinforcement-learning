#
# This code refer: https://github.com/dennybritz/reinforcement-learning/tree/master/MC
#
# For value-prediction, this code refer to: "MC Prediction.ipynb"
# For plot, this code copy: "plotting.py"
#
# By the end, it will plot two figures, one for use-ace-as-1, another for use-ace-as-11

import gym
import sys
import collections
from gym.utils import seeding

import matplotlib
import plotting

matplotlib.style.use('ggplot')

# 1 = Ace, 2-10 = Number cards, Jack/Queen/King = 10
deck = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]

returns_sum   = collections.defaultdict(float)
returns_count = collections.defaultdict(float)
V = collections.defaultdict(float)

def init_player_cards(np_random):
    player_cards = []
    player_cards.append(np_random.choice(deck))
    player_cards.append(np_random.choice(deck))

    # By default, sum < 12 keep hitting    
    sum_of_cards, usable_ace = sum_cards(player_cards)
    while sum_of_cards < 12:
        player_cards.append(np_random.choice(deck))
        sum_of_cards, usable_ace = sum_cards(player_cards)

    print "init player card: {}, sum_of_cards: {}, usable_ace: {}".format(player_cards, sum_of_cards, usable_ace)
    return player_cards

def init_dealer_cards(np_random):
    dealer_cards = []

    dealer_cards.append(np_random.choice(deck))
    dealer_cards.append(np_random.choice(deck))
    
    print "init dealer card: {}".format(dealer_cards)
    return dealer_cards

def generate_random_func(seed):
    np_random, seed = seeding.np_random(seed=None)
    return np_random

# True/False: use Ace as 10/use Ace as 1
# sum: sum cards
# case:
# cards = [1, 2, 1], expected result: 15, True
# if next, cards = [1, 2, 1, 9], expected result: 13, False
def sum_cards(cards):
    if 1 in cards and sum(cards) + 10 <= 21:
        return sum(cards) + 10, True
    else:
        return sum(cards), False

def policy(sum_of_cards):
    if sum_of_cards < 20:
        return 1    # hit
    else:
        return 0    # stick

def hit(np_random, cards):
    cards.append(np_random.choice(deck))
    return cards

#
# blackjack 'state' & 'episode'
#   'state': is a tuple - (sum_of_player_cards, first_dealer_card, use_ace)
#   'episode': is a tuple of tuple - ((state_0, action_0, reward_0), (state_1, action_1, reward_1), ...)
#
# return:
# episode: [((15, True, 9), 1, 0), ((12, False, 9), 1, 0), ((18, False, 9), 1, -1)]
#
def blackjack(np_random, init_player_cards, init_dealer_cards):
    episode = []

    player_cards = init_player_cards
    dealer_cards = init_dealer_cards
    
    # initial state:    
    sum_of_player_cards, use_ace = sum_cards(player_cards)
    
    assert sum_of_player_cards >= 12 and sum_of_player_cards <= 21

    while sum_of_player_cards <= 21:    # continue play 
        print "PLAY ..."
        #state = (sum_of_player_cards, use_ace, dealer_cards[0])
        state = (sum_of_player_cards, dealer_cards[0], use_ace)

        action = policy(sum_of_player_cards)

        if action == 1:
            # Player Hit
            player_cards = hit(np_random, player_cards)    # Take Action 
            sum_of_player_cards, use_ace = sum_cards(player_cards)  # Reach a new State
            
            # Receive the reward
            if sum_of_player_cards > 21:
                print "action-1, > 21, player_cards: {}, sum: {}".format(player_cards, sum_of_player_cards)
                reward = -1     
                episode.append((state, action, reward)) 
            else:
                print "action-1, < 21, player_cards: {}, sum: {}".format(player_cards, sum_of_player_cards)
                reward = 0
                episode.append((state, action, reward)) 
        else:
            assert sum_of_player_cards == 20 or sum_of_player_cards == 21 
            
            sum_of_dealer_cards, _ = sum_cards(dealer_cards)           
            
            # Make sure sum of dealer >= 17
            while sum_of_dealer_cards < 17:            
                dealer_cards = hit(np_random, dealer_cards)
                sum_of_dealer_cards, _ = sum_cards(dealer_cards)
            
            print "dealer_cards: {}, sum_of_dealer_cards: {}, sum_of_player_cards: {}".format(dealer_cards, sum_of_dealer_cards, sum_of_player_cards)

            if sum_of_dealer_cards > 21 or sum_of_dealer_cards < sum_of_player_cards:
                print "action-0, > 21 or dealer_cards < player_cards"
                reward = 1
                print "state: {}, action: {}, reward: {}".format(state, action, reward) 
                episode.append((state, action, reward)) 
                break
            elif sum_of_dealer_cards == sum_of_player_cards:
                print "action-0, dealer_cards = player_cards"
                reward = 0
                episode.append((state, action, reward)) 
                break
            elif sum_of_dealer_cards > sum_of_player_cards:
                print "action-0, dealer_cards > player_cards"
                reward = -1
                episode.append((state, action, reward)) 
                break
    
    for index, value in enumerate(episode):
        state = value[0] 
        print "state is: {}".format(state)
        returns_count[state] += 1

        for i, v in enumerate(episode[index:]):
            returns_sum[state] += v[2]
    
        print "returns_count[state]: {}, returns_sum[state]:{}".format(returns_count[state], returns_sum[state]) 
        V[state] = returns_sum[state] / returns_count[state]

    return episode

if __name__ == '__main__':
    
    loop_time = 500000
    index = 0
    for loop in range(loop_time):
        index = index + 1
        random_func = generate_random_func(None) 
        player_cards = init_player_cards(random_func)
        dealer_cards = init_dealer_cards(random_func)
        episode = blackjack(random_func, player_cards, dealer_cards)
        print "index: {}, episode: {}\n".format(index, episode)
    
    print "value-state: {}".format(V) 
    plotting.plot_value_function(V, title="500000_Steps")
