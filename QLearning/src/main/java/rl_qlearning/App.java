package rl_qlearning;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int[][] rewards = new int[6][6];
        double[][] q = new double[6][6];

        for(int r = 0; r < 6; r++){
            for(int c = 0; c < 6; c++){
                rewards[r][c] = -1;
                q[r][c] = 0;
            }
        }

        rewards[0][4] = 0;

        rewards[1][3] = 0;
        rewards[1][5] = 100;

        rewards[2][3] = 0;

        rewards[3][1] = 0;
        rewards[3][2] = 0;
        rewards[3][4] = 0;

        rewards[4][0] = 0;
        rewards[4][3] = 0;
        rewards[4][5] = 100;

        rewards[5][1] = 0;
        rewards[5][4] = 0;
        rewards[5][5] = 100;
        qLearning(rewards, q, 10000, 0.8);
        System.out.println( "Hello World!" );
    }

    public static void qLearning(int[][]R, double[][]Q, int iteration, double gamma){

        for(int i = 0; i < iteration; i++){ //iteration
            for(int currState = 0; currState < 6; currState++){ // Current state
                for(int nextState = 0; nextState < 6; nextState++){ //nextState
                    if(R[currState][nextState] != -1){
                        double maxQ = findMaxQ(Q, nextState);
                        Q[currState][nextState] = R[currState][nextState] + gamma*maxQ;
                    }
                }
            }
            System.out.format("--- %d iteration res: ---\n", i+1);
            printQ(Q);
        }
    }

    public static double findMaxQ(double[][]Q, int row){
        double maxQ = -1;

        for(int col = 0; col < 6; col++){
            if(Q[row][col] > maxQ){
                maxQ = Q[row][col];
            }
        }

        return maxQ;
    }

    public static void printQ(double[][] Q){
        for(int row = 0; row < 6; row++){
            for(int col = 0; col < 6; col++){
                System.out.print(Q[row][col] + " ");
            }

            System.out.println("\n");
        }
    }
}
